# Storytelling

* [Kurs als Ebook](https://mastecker.gitlab.io/storytelling/course.epub)
* [Kurs als PDF](https://mastecker.gitlab.io/storytelling/course.pdf)
* [Kurs als HTML](https://mastecker.gitlab.io/storytelling/index.html)
