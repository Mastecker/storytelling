# „Storytelling“
#### ...die ursprünglichste Form der Wissensvermittlung
Mai 2017



### Was ist Storytelling?
Der Begriff Storytelling ins Deutsche übersetzt bedeutet etwa „Geschichten erzählen“. Beim Storytelling handelt es sich um eine Erzählmethode, durch die implizites und explizites Wissen weitergegeben und durch Zuhören aufgenommen wird. Die Zuhörenden werden meist in die Erzählung der Geschichte mit eingebunden, sodass sie aktiv daran teilhaben und sich die vermittelten Inhalte besser merken können. 
Werden die Inhalte nüchtern in Form einer Ansprache oder eines normalen Vortrags vermittelt, ist es häufig schwieriger die Aufmerksamkeit und Konzentration der Zuhörenden zu bekommen und zu halten. Verpackt der Vortragende die Inhalte in eine Geschichte, baut einen Spannungsbogen und eine Pointe ein, so fällt es den Zuhörenden leichter, dem Erzählten zu folgen und aufmerksam zu sein. So kann Storytelling konkret definiert werden: „Storytelling heißt Geschichten gezielt, bewusst und gekonnt einzusetzen, um wichtige Inhalte besser verständlich zu machen, um das Lernen und Mitdenken der Zuhörer nachhaltig zu unterstützen, um Ideen zu streuen, geistige Beteiligung zu fördern und damit der Kommunikation eine neue Qualität hinzuzufügen“ (Frenzel, Müller & Sottong, 2006, S. 3).
Neben dem Erzählten ist besonders auch die Mimik, Gestik und die Stimme ein weiteres Ausdrucksmittel. Die Art und Weise, wie die Geschichte erzählt wird, sollte der Zielgruppe, wie z.B. den Schülern und Studierenden angepasst sein.
Storytelling ist eine, besonders in der Unternehmensentwicklung, empirisch untersuchte Methode und gilt als erfolgreich, weil sie an die Grundprinzipien des Gehirns anknüpft. Es handelt sich beim Geschichten erzählen also um eine „gehirngerechte“ Kommunikation, weil das Gehirn mit einem Datenverarbeitungsprogramm verglichen werden kann, welches Informationen aufnimmt, verarbeitet und speichert. Die bildhafte und anschauliche Darstellungsweise von Geschichten sind besonders wirkungsvoll und lassen sich vom Gehirn gut aufnehmen („innere Bilder“). Im Hinblick auf die Evolution wird deutlich, dass Menschen schon immer Geschichten erzählt haben. Dadurch entstandene Sagen und Mythen (wie z.B. die griechische Mythologie) sind bis heute in den Köpfen präsent und sehr wirkungsvoll.

### Zu welchem Zweck?
Das Storytelling dient zum einen der Unterhaltung durch den Erzählenden, aber auch der Wissensvermittlung. Die Methode findet daher in der schulischen, aber auch Erwachsenenbildung und in Unternehmen, insbesondere dem Marketingbereich, Anwendung.
Im Marketing Bereich ist besonders von Digital Brand Storytelling die Rede. Damit ist das Erzählen von Markengeschichte mithilfe von digitalen Techniken gemeint. Ziel ist es, die Marke gezielt durch das Erzählen von Geschichten zu inszenieren. Die dahinterstehende Technik beinhaltet drei Aspekte: Was erzählt die Marke (Handlung), wie wird dies erzählt (Darstellung) und wozu erzählt sie etwas (Wirkung). Übergeordnetes Ziel ist es, die Marke in den Köpfen der Zielgruppen präsent zu halten und dadurch bekannt zu machen. In den Köpfen der Bezugsgruppe soll ein Vorstellungsbild (Markenimage) entstehen, an welches sie sich schnell und einfach erinnern und was dazu führt, dass sie sich für diese Marke entscheiden. Das traditionelle Geschichtenerzählen wird im Digital Brand Storytelling genutzt und um digitale Technologien ergänzt. Die Geschichten können auf einer Vielzahl von Geräten und in einer Vielzahl an Darstellungsformen (Multimedialität) abgebildet werden. Die Zugänglichkeit, Verfügbarkeit und Vernetzung kann genutzt werden, um die Zielgruppe im größten Umfang zu erreichen (Herbst, 2014).

### Wie geht man vor?

Zunächst sollte man Storytelling, als Erzählen und Zuhören, als Kommunikationskultur anerkennen. Man sollte sich bewusst dafür entscheiden, Geschichten und damit das Erzählte in die Kommunikation einfließen zu lassen und aktiv zu nutzen. 
Um dann Storytelling erfolgreich zu nutzen, ist ein gewisses Grundwissen über Geschichten notwendig. Was macht eine Geschichte aus? Was unterscheidet eine Geschichte von anderen Kommunikationsformen?
Die fünf Erfolgsbausteine, die das Rezept für gute Geschichten sind (Sammer, 2014):
1.	Jede gute Geschichte hat einen Grund, erzählt zu werden. Das bedeutet: Definieren Sie erst Ihre Werte im Arbeitsbereich. Und fahnden Sie dann nach Geschichten, die Ihre „Marke“ widerspiegeln. Ein Fitness-Studio, das auf toughe Workouts für die Traumfigur setzt, erzählt eine andere Geschichte als ein Studio, bei dem das entspannte Relaxen im Mittelpunkt steht.
2. Jede gute Geschichte hat einen Helden oder eine Heldin (und einen Antihelden). Mit Persönlichkeit und Charakter. Wichtig: Um Produkt und Hersteller geht es dabei nur am Rand.
3. Jede gute Geschichte erzählt einen Konflikt. Weil alles andere langweilig ist. Dass das auch in aller Kürze geht, zeigen kleine 3-Wort-Geschichten, mit denen ein Gartencenter für sich wirbt („Orchidee verführt Kaktus“).
4.	Jede gute Geschichte baut eine Gefühlswelt auf: Sie lockt mit Bildern und Worten, weckt Neugier, erzeugt Spannung oder fesselt mit einer überraschenden Wendung. Als Beispiel für besonders berührendes Storytelling nennt Petra Sammer die Filme der „Cleveland Clinic “.
5.	Gute Geschichten werden weitererzählt. Früher am Lagerfeuer. Heute im Netz. Wobei heute noch mehr als damals gilt: Die Masse macht‘s. Erfolgreiche Geschichten werden weitererzählt, geteilt und gelikt. Etwa LEGO® kann dabei behilflich sein, denn von 10 Leuten haben 9 der Marke ein positives Gefühl gegenüber und sind motivierter Inhalte dazu in social networks zu teilen.

Dieser Spannungsbogen ist essentiell und wird vom ersten Moment an kontinuierlich aufgebaut – bis zum Schluss. Danach besitzt eine funktionierende Story idealerweise fünf klassische Elemente:

1.	Eine emotional bedeutende Ausgangssituation.
2.	Eine (sympathische) Hauptfigur.  
3.	Konflikte und Hindernisse, die die Hauptfigur überwinden muss.
4. Eine erkennbare Entwicklung (Vorher-Nachher-Effekt).
5. Und einen Höhepunkt, möglichst ein auf das eigene Leben anwendbares Fazit – die Moral von der Geschichte.

### Welche Gedanken sollte ich mir im Vorfeld machen?

- Machen Sie sich zuerst klar, warum Sie diese Geschichte erzählen wollen. Was ist Ihre Botschaft? Was möchten Sie Ihrem Publikum mit auf den Weg geben? Das sind die Fragen, die Sie beantworten sollten, damit Ihre Geschichte nicht nur eine hohle Aneinanderreihung von Worten bleibt.
- Die Geschichte muss zu Ihnen oder Ihrer Institution passen. Ihre Geschichte sollte authentisch sein. Sie sollte Ihre Persönlichkeit und Ihre Werte widerspiegeln. Versuchen Sie nicht, sich zu verstellen, denn das fällt dem Publikum eher früher als später auf.
- Machen Sie sich angreifbar. Um eine gute Geschichte zu erzählen, müssen Sie etwas von sich preisgeben. Teilen Sie Ihre Ängste und Sorgen mit dem Publikum. Nur so geben Sie dem Publikum die Chance, sich mit der Situation auch emotional zu identifizieren. Auch wenn es Mut erfordert: Denken Sie daran, dass niemand sich mit einem perfekten Menschen identifizieren kann. In jeder guten Geschichte findet sich der Zuhörer ein Stück weit selbst wieder.
-  Sprechen Sie das Herz Ihres Publikums an. Eine wirklich gute Geschichte provoziert emotionale Reaktionen beim Publikum. Ob sie lachen, weinen oder wutentbrannt aufschreien - wichtig ist, dass sie die Geschichte mitreißt. Emotionen verbinden die Zuhörer miteinander. Wer gemeinsam lacht, fühlt sich für einen kurzen Moment seinem Nachbarn verbunden. Die Kunst liegt darin, die emotionalen Bedürfnisse des Publikums zu befriedigen.
- Sprechen Sie auch den Verstand Ihres Publikums an. Es ist ein Irrtum, dass eine Geschichte nur dazu da ist, das Publikum zu bespaßen. Eine gute Geschichte regt immer auch zum Denken an, sie bietet neue Einsichten und Orientierung. Sie vermittelt ein Aha-Erlebnis, das der Zuhörer mit nach Hause nehmen und weitererzählen kann (Hättest du gewusst, dass ...?).
- Holen Sie Ihr Publikum ab. Ein guter Geschichtenerzähler holt seine Zuhörer auf Augenhöhe ab. Er muss Empathie vermitteln und zeigen: Ich kenne dich und deine Situation. Idealerweise geschieht dies direkt mit dem Einstieg. Daher ist es auch so wichtig, sein Publikum zu kennen und sich Gedanken darüber zu machen, wem Sie die Geschichte erzählen. Was weiß das Publikum bereits? Was möchte es wissen?
- Erfüllen Sie Ihr Versprechen. Nichts ist schlimmer, als die Erwartungen des Publikums nicht zu erfüllen. Stellen Sie beispielsweise eine Frage oder wollen Sie ein Problem erläutern, gibt es kaum etwas Frustrierenderes für den Zuhörer, als die Antwort am Ende nicht zu bekommen. Das Publikum schenkt Ihnen Zeit, damit Sie Ihre Geschichte erzählen können. Vermeiden Sie es, dass der Zuhörer den Eindruck hat, diese Zeit sei verschwendet gewesen.
- Lassen Sie das Publikum an der Geschichte teilhaben. Markante Geschichten sind immer auch interaktiv aufgebaut und lassen den Zuhörer oder Zuschauer (wenigstens gedanklich) Teil der Handlung werden. Sicher, damit geben Sie einen Teil der Kontrolle ab, doch Sie gewinnen einen Zuhörer, der sich besser in die Situation hineindenken und sich mit dieser identifizieren kann. Gerade das Internet bietet hierbei ideale Voraussetzungen und Optionen.

### Storytelling in der Schule/Universität
Der Einsatz der Storytelling Methode in der Schule (gleiches gilt für die Universität) lässt sich aus verschiedenen Perspektiven begründen. 
Zum einen sind die Adressaten die Schüler, also Kinder und Jugendliche, die gerne zuhören und Geschichten mögen. So können die Lerninhalte vermittelt, aber vor allem auch die Schüler motiviert werden. Geschichten lösen Emotionen aus, führen zu Gefühlen, die den Zuhörer ermöglichen in die Geschichte einzutauchen. Das Zuhören einer Geschichte erfordert und trainiert zusätzlich die Konzentrationsfähigkeit und Vorstellungskraft. 
Besonders im Fremdsprachenunterricht oder schwierigen Sachverhalten kann das Erzählen einer Geschichte, durch den unterstützenden Einsatz von Mimik, Gestik und ggf. Bildern, das Verständnis erleichtern. 
Storytelling kann auch dazu führen, dass eine Verbindung zwischen dem realen Alltagsumfeld und der Fiktion und reinen Vorstellungskraft erfolgen kann. Dadurch, dass man sich mit Personen der Geschichte identifiziert, sich versucht in sie hineinzuversetzen und jeder Teil der Geschichte werden kann, wird die Vorstellungskraft und das leichtere Verständnis der Inhalte unterstützt.
Zusätzlich führt das Erzählen einer Geschichte zu einer abwechslungsreichen und entspannten Unterrichtsatmosphäre. Das Lernen erfolgt indirekt, ohne, dass sich die Schüler wirklich darüber bewusst sind, dass es sich um einen Lernprozess handelt. Zudem stärken eine solche gemeinsame Erfahrung und das Teilen von Emotionen den Zusammenhalt einer Lerngruppe.
Ein weiterer Grund für den Einsatz dieser Methode im Unterricht ist, dass die Lesemotivation gefördert werden kann. Besonders in Anbetracht der Digitalisierung ist es umso bedeutsamer, dass die Kinder und Jugendliche noch sehen, wieviel Spaß es machen kann, einer Geschichte zuzuhören oder eine zu lesen.

#### Beispiele
MOOCs (Massive Open Online Courses) zu Storytelling:
1. Khan Academy und “Pixar in a Box”: https://www.khanacademy.org/partner-content/pixar/storytelling
2. FH Potsdam produzierte einen offenen Kurs:
https://www.fh-potsdam.de/forschen/projekte/projekt-detailansicht/project-action/the-future-of-storytelling-mooc/



##### Quellen:
- Frenzel, K., Müller, M. & Sottong, H. (2006). Storytelling. Das Praxisbuch. München/Wien: Hanser Verlag.
- Fuchs, Werner T. (2015). Warum das Gehirn Geschichten liebt. Mit Storytelling Menschen gewinnen und überzeugen. 3. Aufl. Freiburg: Haufe Lexware Verlag.
- Herbst, Dieter G. (2014). Digital Brand Storytelling – Geschichten am digitalen Lagerfeuer? In S. Dänzler & T. Heun, Marke und digitale Medien (S. 223-241). Wiesbaden: Springer Verlag.
- Sammer, Petra (2014). Storytelling. Die Zukunft von PR und Marketing. O’Reilly Verlag.
- http://karrierebibel.de/storytelling/
- http://www.gedankentanken.com
